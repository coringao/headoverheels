HEAD OVER HEELS
===============

Two distinct characters called Mr Head and Mr Heels are played
in this arcade adventure. They are separated on the shoulders,
but are able to come together to maximize their abilities
in some situations (but reduce them in others, so that they can divide again).

The game is presented from an isometric perspective and presents the characters
with a series of puzzles involving switches, jumps and climbs.
The levels are three-dimensional and the characters must collect crowns
as they progress. Objects can be picked up and manipulated as needed.
