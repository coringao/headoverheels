#if defined( USE_ALLEGRO5 ) && USE_ALLEGRO5
#  include <allegro5/allegro.h>
#elif defined( USE_ALLEGRO4 ) && USE_ALLEGRO4
#  include <allegro.h>
#endif
#ifdef __WIN32
  #include <winalleg.h>
#endif
#include "GuiManager.hpp"

int main()
{
#ifdef __WIN32
  timeBeginPeriod(1);
#endif
  
  gui::GuiManager::getInstance()->start();

#ifdef __WIN32
  timeEndPeriod(1);
#endif

  return 0;
}
END_OF_MAIN()
